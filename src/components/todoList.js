import React, { useState, useRef } from 'react';
import List from './list';
import { useSelector, useDispatch } from 'react-redux';
import Container from 'react-bootstrap/Container';
import Jumbotron from 'react-bootstrap/Jumbotron';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';

export default function TodoList() {
    const { item, list } = useSelector((state) => state);
    const dispatch = useDispatch();
    const myInput = useRef();

    function handleChangeInput(e) {
        dispatch({
            type: 'CHANGE_INPUT',
            payload: e.target.value
        });
    }

    function handleAddButton() {
        if (myInput.current.value) {
            dispatch({
                type: 'ADD_BUTTON',
                payload: myInput.current.value
            });
        }
    }

    function handleKeyPress(e) {
        if (e.key === 'Enter' && myInput.current.value) {
            dispatch({
                type: 'ADD_BUTTON',
                payload: myInput.current.value
            });
        }
    }

    function handleDeleteItem(id) {
        dispatch({
            type: 'DELETE_ITEM',
            payload: id
        });
    }

    function handleOnComplete(id) {
        dispatch({
            type: 'COMPLETE_ITEM',
            payload: id
        })
    }

    return (
        <Container>
            <Jumbotron>
                <InputGroup className="justify-content-md-center">
                    <FormControl
                        type="text"
                        value={item}
                        ref={myInput}
                        onChange={e => handleChangeInput(e)}
                        onKeyPress={e => handleKeyPress(e)}
                        placeholder="Tasks"
                        aria-label="Recipient's username"
                        aria-describedby="basic-addon2"
                    />
                    <InputGroup.Append>
                        <Button variant="success" type="button" onClick={handleAddButton}>
                            ADD
                        </Button>
                    </InputGroup.Append>
                </InputGroup>
                <br />
                <div>
                    <List list={list} deleteItem={handleDeleteItem} onComplete={handleOnComplete} />
                </div>
            </Jumbotron>
        </Container>
    )
}






// export default function TodoList() {
//     const [item, setItem] = useState('');
//     const [list, setList] = useState([]);

//     function handleChangeInput(e) {
//         setItem(e.target.value)
//     }

//     function handleAddButton() {
//         setList([...list, {
//             name: item,
//             id: Math.random().toString()
//         }])
//         setItem('');
//     }

//     // function handleCancelButton() {
//     //     setItem('');
//     //     document.textContent = '';
//     // }

//     function handleDeleteItem(id) {
//         const deleteItemIndex = list.findIndex(item => item.id === id)
//         const newList = list.slice(0, deleteItemIndex)
//         const newlist2 = list.slice(deleteItemIndex + 1)
//         setList([...newList, ...newlist2]);
//     }

//     return (
//         <div>
//             <input class="" type="text" placeholder="Tasks" value={item} onChange={handleChangeInput} />
//             <button class="" type="button" onClick={handleAddButton}>ADD</button>
//             {/* <button class="" type="button" onClick={handleCancelButton}>Cancel</button> */}
//             <div>
//                 <List list={list} deleteItem={handleDeleteItem} />
//             </div>
//         </div>
//     )
// }