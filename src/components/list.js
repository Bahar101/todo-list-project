import React from 'react';
import ListItem from './listItem';
import ListGroup from 'react-bootstrap/ListGroup';

export default function List({ list, deleteItem, onComplete }) {
    return (
        <ListGroup variant="flush">
            {list.map(item => {
                return <ListItem
                    key={item.id}
                    name={item.name}
                    id={item.id}
                    isComplete={item.isComplete}
                    deleteItem={() => deleteItem(item.id)}
                    onComplete={() => onComplete(item.id)}
                />
            }
            )}
        </ListGroup>
    )
}