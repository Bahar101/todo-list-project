import React, { useState } from 'react';
import { ListGroup } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import './listItem.css';

export default function ListItem({ name, isComplete, deleteItem, onComplete }) {
    return (
        <ListGroup.Item>
            <span 
            onClick={onComplete} 
            className={isComplete ? 'done' : undefined}
            >
                {name}
            </span>
            <Button variant="danger" size="sm" onClick={deleteItem} className={'xbtn'}>X</Button>
        </ListGroup.Item>
    )
}