import { createStore } from 'redux';
import { reducer } from '../stateManager/reducer';

export default createStore(reducer);