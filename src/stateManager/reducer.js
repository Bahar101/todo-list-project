const initState = {
    item: '',
    list: []
}

export function reducer(state = initState, action) {
    switch (action.type) {
        case 'CHANGE_INPUT':
            return {
                ...state,
                item: action.payload
            }
        case 'ADD_BUTTON':
            return {
                ...state,
                list: [...state.list, {
                    name: action.payload,
                    id: Math.random().toString(),
                    isComplete: false
                }],
                item: ''
            }

        case 'COMPLETE_ITEM':
            console.log();
            const completeItem = state.list.find(item => item.id === action.payload);
            const completeItemIndex = state.list.findIndex(item => item.id === action.payload);
            const list1 = state.list.slice(0, completeItemIndex);
            const list2 = state.list.slice(completeItemIndex + 1);
            if (completeItem.isComplete) {
                completeItem.isComplete = false;
            } else {
                completeItem.isComplete = true;
            }

            return {
                ...state,
                list: [...list1, completeItem, ...list2]
            }

        case 'DELETE_ITEM':
            const deleteItemIndex = state.list.findIndex(item => item.id === action.payload);
            const newList1 = state.list.slice(0, deleteItemIndex);
            const newList2 = state.list.slice(deleteItemIndex + 1);
            return {
                ...state,
                list: [...newList1, ...newList2]
            }

        default:
            return state;
    }
}